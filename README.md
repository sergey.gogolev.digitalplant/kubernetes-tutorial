# Запуск локального кластера minikube

Для целей отладки и разработки есть возможность запустить
kubernetes внутри локальной вирутальной машины используя всего
одну команду.
Для этого используется приложение под названием
minikube (https://github.com/kubernetes/minikube/releases).
После скачивания необходимо скопировать его в директорию,
которая есть в PATH и далее в консоли написать `minikube start`.
По умолчанию кластер развернется внутри
VirtualBox(установить отдельно).
При необходимости можно запустить minikube внутри hyper-v.

Minikube также настраивает клиентскую часть ([kubectl](https://storage.googleapis.com/kubernetes-release/release/v1.12.0/bin/windows/amd64/kubectl.exe))
(файл `%HOMEPATH%\.kube\config`)


Проверить что кластер запущен можно командой `kubectl cluster-info`
```
Kubernetes master is running at https://192.168.99.100:8443
CoreDNS is running at https://192.168.99.100:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

# Основные понятия
Kubernetes дает возможность управлять развертыванием приложения в стиле `Desired state`.
Кластер Kubernetes состоит из следующих основных компонентов:
- kube-apiserver – сервис предоставляющий restful api для конфигурирования и управления.
- Etcd – распределенное хранилище конфигурации
- Controller manager, Scheduler – серверные компоненты, отвечающие за правильную работу планировщика подов и контроллеров (StatefulSet, DaemonSet). Осуществляют планирование запуска подов в kubelet.
- Kubelet – планировщик подов, который выполняет задачи по развертыванию подов и контейнеров, а также удалению, сбору логов и т.д.
- Kube-proxy – приложение которое предоставляет сетевой доступ к поду из внешней сети. По сути это обратный прокси сервер, который переадресовывает запросы на нужный адрес внутри сети подов.

![k8s](/img/k8s.png)

Под - основная единица запуска в kubernetes. Абстракция надо контейнером,
представляет собой группу контейнеров(часто один) запускаемый на узлах кластера.
Часто поды запускаются не отдельно а в составе контроллеров - replicaset,statefulset,daemonset.

# Запуск приложения
Для примера запустим вручную приложение:
```
kubectl.exe run test --image=cizixs/whoami --env="MESSAGE=test" --port=8778
kubectl.exe expose deployment test --type=NodePort
start $(minikube.exe service --url=true test)

kubectl.exe scale --replicas=2 deployment/test
```
Удаление развертывания:
```
kubectl.exe delete svc/test
kubectl.exe delete deploy/test
``` 
# Пример развертывания
Для запуска приложений используются развертывания в формате yaml.
Возьмем этот пример https://github.com/Azure-Samples/azure-voting-app-redis
Изменим в нем тип сервиса с LoadBalancer на NodePort и запустим в кластере.
(Тип сервиса LoadBalancer не поддерживается minikube).
```
kubectl.exe apply -f ./azure-vote-nodeport.yml
start $(minikube.exe service --url=true azure-vote-front)
```
Удалить развертывание можно также используя файл, с командой delete:
```
kubectl.exe delete -f ./azure-vote-nodeport.yml
```


